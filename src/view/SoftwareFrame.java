package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SoftwareFrame extends JFrame{
private static final double DEFAULT_RATE = 5;
	
	private JLabel showLabel;
	private JTextField showField;
	private JButton showButton;
	private JTextArea showTextArea;
	private JComboBox comboBox;

	
	public SoftwareFrame()
	{
		Frame();

	}
	public void Frame(){
		setSize(300, 480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(null);
		
		showTextArea = new JTextArea();

		showTextArea.setBounds(120,120, 5, 100);
		showTextArea.setEditable(false);

		
		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBounds(13,95,270,340);
		panel1.setLayout(new GridLayout(1,2));
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(null);
		panel2.setBounds(40,50,100,100);
		
		/*JComboBox comboBox = new JComboBox();
		comboBox.setLayout(null);
		comboBox.addItem("model 1");
		comboBox.addItem("model 2");
		comboBox.addItem("model 3");
		comboBox.addItem("model 4");
		comboBox.addItem("model 5");*/
		
		
		String[] model = { "Model 1", "Model 2", "Model 3", "Model 4", "Model 5" };
	    comboBox = new JComboBox(model);
	    comboBox.setBounds(0,0,100,22);
	    comboBox.setForeground(Color.BLUE);
	    comboBox.setFont(new Font("Book Antiqua", Font.BOLD, 14));
	    //comboBox.setEnabled(true);
	  
		showLabel = new JLabel("Number : ");
		showLabel.setBounds(5,20,100,22);
		
		showField = new JTextField();
		showField.setText("");	
		showField.setBounds(60,20,180,22);
		
		showButton = new JButton("run");
		showButton.setBounds(180,50,60,22);
		
		add(this.showLabel);
	    add(this.showField);
	    add(this.showButton);
	    
		panel1.add(showTextArea);
		panel2.add(comboBox);
	    add(panel1);
	    add(panel2);
	    setVisible(true);
		setResizable(false);
	}
	public String getItem()
	{
		String smodel = comboBox.getSelectedItem().toString();
		return smodel;
	}
	
	
	public JTextArea getTextArea()
	{
		return showTextArea;
	}
	
	public JTextField getField()
	{
		return showField;
	}
	
	public JButton getButton()
	{
		return showButton;
	}
	
	
}
