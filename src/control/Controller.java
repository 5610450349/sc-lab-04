package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.SoftwareFrame;
import model.Nested;
import model.Main;

public class Controller {
	private Nested nt ;
	private SoftwareFrame view ;
	private ActionListener actionListener ;
	private String text = "";
	
	public Controller(Nested nt,SoftwareFrame view){
		this.nt = nt;
		this.view = view;
	}
	public void Listener()
	{
		actionListener = new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				String item = view.getItem();
				int size = Integer.parseInt(view.getField().getText());
				nt.star(item,size);
				text += nt.star(item,size)+"\n";
				view.getTextArea().setText(text);
				
			}
		};
		view.getButton().addActionListener(actionListener); 
	}
}