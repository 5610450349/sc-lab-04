package model;

public class Nested {
	String sb = "";
	public String star(String item,int size) {
        int i,j;
        String sb = "";
       
        if (item == "Model 1") {
            for(i=1; i<=size; i++){
            	for(j=1; j<=size+1; j++){
            		sb += "*";
				}
            	sb += "\n";
			}
			return sb;
        }
        else if (item == "Model 2") {
        	for(i=1; i<=size; i++){
            	for(j=1; j<=size-1; j++){
            		sb += "*";
				}
            	sb += "\n";
			}
			return sb;
            
        }
        else if (item == "Model 3") {
        	for(i=1; i<=size; i++){
            	for(j=1; j<=i; j++){
            		sb += "*";
				}
            	sb += "\n";
			}
			return sb;
            
        }
        else if (item == "Model 4") {
        	for(i=1; i<=size; i++){
            	for(j=1; j<=size+2; j++){
            		if(j%2==0){
            			sb += "*";
    				}
            		else{
            			sb += "-";
            		}
            	}
            	sb += "\n";
            }
        	return sb;
        }
        else if (item == "Model 5") {
        	for (i=1;i<=size;i++){
				for (j=1;j<=size+2;j++){
					if ((i+j)%2==0){
						sb += "*";
						}
					else {
						sb += " ";
						}
				}
				sb += "\n";
			}
			return sb;
        }
		return sb;		
	}
}
