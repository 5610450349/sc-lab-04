package model;

import view.SoftwareFrame;
import control.Controller;

public class Main {
	public static void main(String[] args){
		SoftwareFrame s = new SoftwareFrame();
		Nested n = new Nested();
		Controller c = new Controller(n,s);
		c.Listener();
	}
}
